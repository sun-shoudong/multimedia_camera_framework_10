# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("./../../../../../multimedia_camera_framework.gni")

module_output_path = "camera_framework/camera_framework_unittest"

ohos_unittest("camera_framework_unittest_v1_1") {
  module_out_path = module_output_path
  include_dirs = [
    "./include",
    "${graphic_surface_path}/surface/include",
    "${multimedia_camera_framework_path}/services/camera_service/include",
    "${multimedia_camera_framework_path}/services/camera_service/include/avcodec",
    "${multimedia_camera_framework_path}/services/camera_service/include/avcodec/common",
    "${multimedia_camera_framework_path}/services/camera_service/binder/base/include",
    "${multimedia_camera_framework_path}/services/camera_service/binder/client/include",
    "${multimedia_camera_framework_path}/services/camera_service/binder/server/include",
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/camera/include",
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/test",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/task_manager",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/task_manager/task_group",
    "${base_security_path}/interfaces/innerkits/accesstoken/include",
    "${base_security_path}/interfaces/innerkits/token_setproc/include",
    "${multimedia_media_library_path}/interfaces/kits/c",
  ]

  sources = [
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/test/test_common.cpp",
    "src/v1_1/camera_framework_unittest.cpp",
  ]

  deps = [
    "${multimedia_camera_framework_path}/frameworks/native/camera:camera_framework",
    "${multimedia_camera_framework_path}/services/camera_service:camera_service",
    "${third_party_path}:gmock_main",
  ]

  external_deps = [
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libprivacy_sdk",
    "access_token:libtoken_setproc",
    "audio_framework:audio_capturer",
    "audio_framework:audio_client",
    "av_codec:av_codec_client",
    "c_utils:utils",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "drivers_interface_camera:libbuffer_handle_sequenceable_1.0",
    "drivers_interface_camera:libcamera_proxy_1.0",
    "drivers_interface_camera:libcamera_proxy_1.1",
    "drivers_interface_camera:libmap_data_sequenceable_1.0",
    "drivers_interface_camera:metadata",
    "drivers_peripheral_display:hdi_gralloc_client",
    "graphic_surface:surface",
    "hdf_core:libhdi",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "ipc:ipc_core",
    "media_foundation:media_foundation",
    "media_foundation:native_media_core",
    "media_library:media_library",
    "media_library:media_library_manager",
    "os_account:os_account_innerkits",
    "safwk:system_ability_fwk",
    "window_manager:libdm",
  ]

  defines = [ "private=public" ]
  if (use_sensor) {
    external_deps += [ "sensor:sensor_interface_native" ]
    defines += [ "CAMERA_USE_SENSOR" ]
  }

  cflags = [
    "-fPIC",
    "-Werror=unused",
  ]
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
    blocklist = "../../../../../cfi_blocklist.txt"
  }
  include_dirs += [
    "${driver_peripheral_path}/interfaces/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc",
    "${driver_peripheral_path}/interfaces/hdi_ipc/utils/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/server/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/device/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/operator/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/host/include",
  ]

  cflags_cc = cflags
}

ohos_unittest("camera_ndk_unittest_v1_1") {
  module_out_path = module_output_path
  include_dirs = [
    "./include",
    "${multimedia_camera_framework_path}/interfaces/kits/native/include",
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/test",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/task_manager",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/task_manager/task_group",
    "${base_security_path}/interfaces/innerkits/accesstoken/include",
    "${base_security_path}/interfaces/innerkits/token_setproc/include",
    "${multimedia_media_library_path}/interfaces/kits/c",
    "${multimedia_image_framework_path}/interfaces/kits/native/include/image",
  ]

  sources = [
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/test/test_common.cpp",
    "src/v1_1/camera_ndk_unittest.cpp",
  ]

  deps = [
    "${multimedia_camera_framework_path}/frameworks/native/camera:camera_framework",
    "${multimedia_camera_framework_path}/frameworks/native/ndk:ohcamera",
    "${multimedia_camera_framework_path}/services/camera_service:camera_service",
    "${third_party_path}:gmock_main",
  ]

  external_deps = [
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libprivacy_sdk",
    "access_token:libtoken_setproc",
    "av_codec:av_codec_client",
    "c_utils:utils",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "drivers_interface_camera:libbuffer_handle_sequenceable_1.0",
    "drivers_interface_camera:libcamera_proxy_1.0",
    "drivers_interface_camera:libcamera_proxy_1.1",
    "drivers_interface_camera:libmap_data_sequenceable_1.0",
    "drivers_interface_camera:metadata",
    "drivers_peripheral_display:hdi_gralloc_client",
    "graphic_surface:surface",
    "hdf_core:libhdi",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "ipc:ipc_core",
    "media_foundation:media_foundation",
    "media_foundation:native_media_core",
    "media_library:media_library",
    "media_library:media_library_manager",
    "napi:ace_napi",
    "resource_management:librawfile",
    "safwk:system_ability_fwk",
    "sensor:sensor_interface_native",
    "window_manager:libdm",
  ]

  defines = [ "private=public" ]

  cflags = [
    "-fPIC",
    "-Werror=unused",
  ]
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  include_dirs += [
    "${driver_peripheral_path}/interfaces/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc",
    "${driver_peripheral_path}/interfaces/hdi_ipc/utils/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/server/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/device/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/operator/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/host/include",
  ]

  cflags_cc = cflags
}

ohos_unittest("camera_framework_deferred_unittest") {
  module_out_path = module_output_path
  include_dirs = [
    "./include",
    "${graphic_surface_path}/surface/include",
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/camera/include",
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/test",
    "${multimedia_camera_framework_path}/services/camera_service/include/avcodec",
    "${multimedia_camera_framework_path}/services/camera_service/include/avcodec/common",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/binder/base/include",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/binder/client/include",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/binder/server/include",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/buffer_manager",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/task_manager",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/task_manager/task_group",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/timer",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/base/timer/core",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/dfx",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/event_monitor",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/post_processor",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/schedule",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/schedule/photo_processor",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/schedule/photo_processor/photo_job_repository",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/schedule/photo_processor/strategy",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/session",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/session/photo_session",
    "${multimedia_camera_framework_path}/services/deferred_processing_service/include/utils",
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/camera/include/utils",
  ]

  sources = [
    "${multimedia_camera_framework_path}/interfaces/inner_api/native/test/test_common.cpp",
    "src/v1_1/camera_deferred_unittest.cpp",
  ]

  deps = [
    "${multimedia_camera_framework_path}/frameworks/native/camera:camera_framework",
    "${multimedia_camera_framework_path}/services/camera_service:camera_service",
    "${multimedia_camera_framework_path}/services/deferred_processing_service:deferred_processing_service",
    "${third_party_path}:gmock_main",
  ]

  external_deps = [
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libprivacy_sdk",
    "access_token:libtoken_setproc",
    "audio_framework:audio_capturer",
    "audio_framework:audio_client",
    "av_codec:av_codec_client",
    "c_utils:utils",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "drivers_interface_camera:libbuffer_handle_sequenceable_1.0",
    "drivers_interface_camera:libcamera_proxy_1.0",
    "drivers_interface_camera:libcamera_proxy_1.1",
    "drivers_interface_camera:libmap_data_sequenceable_1.0",
    "drivers_interface_camera:metadata",
    "drivers_peripheral_display:hdi_gralloc_client",
    "graphic_surface:surface",
    "hdf_core:libhdi",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "ipc:ipc_core",
    "media_foundation:media_foundation",
    "media_foundation:native_media_core",
    "media_library:media_library",
    "media_library:media_library_manager",
    "os_account:os_account_innerkits",
    "safwk:system_ability_fwk",
    "window_manager:libdm",
  ]

  defines = [ "private=public" ]
  if (use_sensor) {
    external_deps += [ "sensor:sensor_interface_native" ]
    defines += [ "CAMERA_USE_SENSOR" ]
  }

  cflags = [
    "-fPIC",
    "-Werror=unused",
  ]
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
    blocklist = "../../../../../cfi_blocklist.txt"
  }
  include_dirs += [
    "${driver_peripheral_path}/interfaces/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc",
    "${driver_peripheral_path}/interfaces/hdi_ipc/utils/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/server/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/device/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/operator/include",
    "${driver_peripheral_path}/interfaces/hdi_ipc/callback/host/include",
  ]

  cflags_cc = cflags
}
